# Lohnt es sich überhaupt ein neues Protokoll für IoT zu entwickeln?
Bisher sind alle Protokolle die ich für IoT gefunden habe scheiße. Es wird bei keinem eine Verschlüsselung der Daten auf Applikationsebene verwendet, es wird nur auf TLS verwiesen.
Auch sind die Datenpakete nicht komprimiert und auch völlig frei in der Gestaltung des Entwicklers.
Die freie Payload Wahl mark zwar gut sein, da sich so alle Idee der Entwickler umsetzten lassen, jedoch wird eine Zusammenarbeit von unterschiedlichen Herrstellern nur über Bridges erwirkt.
Mit D-IoT möchte ich eine gute Alternative für die Erfassung von Sensordaten bieten, welche sich vielleicht auf für die Konfiguration einsetzten lässt.

# Warum TLS und DTLS für IoT nicht optimiert ist?
TLS wurde zu absicherung von Applikationen wie SMTP, IMAP, etc entwickelt. All diese Applikationsprotokolle verwenden ein Plainauth für die Authentifizierung.
Das ist für Anwendung mit Nutzern relevant, da man so jedem Nutzer ein einfach merkbaren Nutzernamen zuweisen kann, jedoch für das IoT Umfeld nicht benötigt wird, bei der eine reine M2M Kommunkation im Vordergund steht.
Ein weiters Problem was mit TLS gelöst wurde ist die Identifizierung und Authentifizierung von Servern, mit eine PKI wird primär die Intergrität des Servers sichergestellt.
Bei IoT Geräten welche Sensordaten erfassen sollen ist jedoch auch die Authentifizierung von Geräten relevant und Plainauth ist da keine Lösung.
Weiterhin befindet sich TLS nur auf dem Transportlayer, welcher sich zwar um die Erhaltung von Session kümmern soll, jedoch nicht um die Authentifizierung von Geräten. Dadurch ist bei TLS die Aufrechterhaltung von Sessions und den damit Verbunden Keys nur für die TCP Verbindung relevant.
Eine Implementierung von Sessions, welche länger als die Verbindung halten sollen ist nicht in jeder Implemntierung vorgesehen.

Bei Coap wird bei DTLS eine aktive Verbindung bei jedem Connect zum Server neuaufgebaut.
TLS hat in dem meisten Implementierung auch nur ein Timeout für die Sessions und kann diese nicht exportieren. OpenSSL zum Beispiel.

# AES CCM vs GCM
AES CCM hat den Vorteil, dass die einzelnen Datenpakete mit einem Counter gezählt werden können und die Pakete intern im CBC Mode verschlüsst werden.
Der IV kann daher aus der MessageID bestehen.
Beim GCM wird intern pro Block der Counter erhöht. Dazu müsste der Track mit den letzten Paktenummern sowohl beim Client als auch beim Server aufrecht erhalten werden.
AES CCM ist daher auch einfacher zu implementieren.
TLS 1.3 unterstüzt zwar nur zwangsweise GCM, jedoch soll sowie so kein TLS verwendet werden.

# Therad und Zigbee
Bei diesen beiden Netzwerken ist die Kommuniktaion innerhalb eines Netzwerkes gesichert und verschlüsselt.
Eine E2E Verschlüsselung muss durch die Applikation erfolgen, dass ist zwar sinnvoll, jedoch erhöt das leider den Overhead in jedem Layer. Ähnliches gilt auch für LoRaWAN.

# LoRaWAN
Bei LoRaWAN ist die Verbindung auch auf Netzwerkebene verschlüsselt. Es gibt auch den Ansatz ein AppSKey auszurollen, welcher bei OTAA verteilt wird. Jedoch wird dieser Key von JoinServer/NetworkServer generiert und nicht von der Applikation selber. Das macht diesen Key ein wenig nervig, da er erst durch das Netzwerk verteilt werden muss.

# EuID bei LoRaWAN
Die Geräte selber mussen eine EuID bekommen, welche durch die IEEE vergeben wird, das mag für Unternehmen sinnvoll sein, jedoch ist es für privat Anwender nervig.
Eine 128-Bit UUID welche sich jeder selber würfeln kann klingt hier sinnvoller. 

# Authenfizierung von Geräten und nicht Servern
Ziel soll es sein Geräte Authentifizieren zu können. Daher sollen Geräte eine UUID bekommen, welche mit einem Zertifikat, wie RSA auf dem Geräte beim Erstellen generiert werden kann.
Die Informationen können dann beim Server hinerlegt werden und der Server kann bei einer Anfrage mit diesen Informationen dann einen SessionKey ausgeben, welcher das Geräte mit einer Initial Nachricht mit dem FrameCounter=0 bestätigt.

# DDOS
Da die UUID nur beim Session Erstellen und Erneuern verwendet wird, ist es ein akzeptabler Schutz gegen DDOS, da der Server beim HELLO nur die UUID prüfen muss, ob sie hinterlegt ist. Wenn das nicht der Fall ist, dann kann das Paket einfach gedroppt werden.

Sollte ein Geräte zuoft eine neue Session anfordert, kann die UUID auch gesperrt werden.

Die UUID des Gerätes sollte innerhalb des Servers nicht in öffentlich zugängliche Datenbanken geschrieben werden. Die DevID wäre an der Stelle besser, da sie nur einmal generiert werden muss und so eine Bindung an der Server entsteht.

Einer UUID kann dann auch mehere DevID zugewiesen werden. So können Daten auch aus meheren Sessions vom Server ausgewertet werden.

Wenn das nicht sinnvoll ist, dann könnt man nochmal über eine andere DDoS Protection nachdenken.

# Error Detection
Die Frage ist noch, ob man innerhalb eines Schemas Fehler über die Erzeugung von Sensordaten erstellen möchte, also ob ein Sensorwert verfügbar war. Meiner Ansicht nach sollten keine Schema verwendet werden, die nur teilweise übertragen werden. Als Bespiel gilt hier die Batteriestandsanzeige, der Wert ist deutlich träger als ein Temperaturwert und müsste so nicht bei jedem Update versendet werden. Meiner Meinung nach sollte man hier zwei seperate Schemas definieren, dann kann der Temperaturwert und die Batterie in unterschiedlichen Intervallen übertragen werden.
Bei einer Error Detection wäre es interessant, einen Wert als fehlerhaft zu markieren, wenn zum Beispiel der Sensor ausgefallen ist. Die Frage die sich damit wieder aufdrängt ist, ob man dann auch noch genauere Informationen über den Sensor haben möchte, warum der ausgefallen ist. 

# Datenpakete und Statuspakete
Bei Datenpaketen sollten keine Statusinformationen über das Gerät ansich übertragen werden, sondern nur Werte von Sensoren. Diese Daten sollten bei einer Speicherung in einer Datenbank auch als solche behandelt werden, also persistent und aufgearbeitet.
Bei Statuspaketen könnte man Informationen zum Debuggen oder über den allgemeinen Geräte Status einbringen, Fehlermeldungen müssten nicht persistent vorgehalten werden wie Sensordaten und könnten als Message aus dem System rausfallen.

# NULL Values
Die Notwendigkeit von NULL Values sehe ich nicht, da Daten in einem Schema auch nur zusammen gehörend erhoben werden sollten. Wenn ein Feld nicht vorhanden ist, dann wird die ganze Informationen nicht erhoben. Sollten Sensoren defekt sein, dann sollte eine Error Message geschickt werden.
Wenn dabei Daten verloren gehen, dann wäre ein Errorcode sinnvoller. Der Standard, dass Daten aus einem Schema weggelassen werden sollte nicht eintreten.

# Schema ID Reduktion
Bei dem Handshake zwischen Server und Geräte wird dem Gerät seine verkürtze DeviceID mitgeteilt. In diesem Zusammenhang kann das Geräte auch seine verwenden ShcemaIDs mitteilen. Diese werden dann vom Client durchnummeriert und auf 8 Bit verkürtzt. Mehr als 255 verschieden Schemas auf einem Geräte sind nicht vorgesehen.

# Reduktion um max. 14 Bit bei Error Messages
Lohnt sich nicht, da Error nicht optimiert werden müssen, jedenfalls nicht um maximal 14 Bit.
Die Frage ist auch, ob es die Prozessorzeit Wert ist, da ein weiteres mal über den Payload iteriert werden müsste, oder das beim Erstellen des Payloads berücksichtigt werden müsste.

# LoRaWAN Verschlüsselung
LoRaWANs Verschlüsselung reicht nur bis in das Netzwerk hinein. Man kann zwar ein ApplikationKey setzten, bei OTAA wird der vom Netzwerk generiert und bei ABP ist der zwar dem Netzwerk nicht bekannt, jedoch existiert keine MAC, die den Inhalt der Nachricht verifiziert. Eine MIC gibt es nur im MAC Layer.

# Ascon Key Committing
```
On February 7, 2023, NIST announced the selection of the Ascon family for lightweight cryptography standardization [42]. The finalist version of Ascon [17] specifies two AEAD parameter sets Ascon-128 and Ascon-128a. Both parameter sets specify a 128 bit tag, which by the birthday bound, upper bounds the committing security at 64 bits. But, since the underlying algorithm is a variant of the Duplex construction with a 320-bit permutation, and the same specification specifies parameters for a hash function with 128-bit collision resistance, one can specify an AEAD with 128-bit committing security by tweaking parameters.
```

# Sollte eine ACK Verschlüsselt/Authentifziert sein?
Ein Problem bei der Kommunikation mit UDP ist die Fälschung von Header, speziell dem Header des Absenders. Bei einem ACK könnte theoretisch ein Angreiffer den Erhalt eines Messwerts bestätigen, ohne, dass der Server die Daten erhalten hat. 
Bei einer verschlüsselten Kommunikation müsste der FrameCounter erhöht werden.
Das wäre akzeptabel, da der Server alle Nachrichten und Typen loggen könnte und so Datenpunkte von anderen Paketen unterscheiden könnte.

# Probleme des FrameCounters als Data Counter
Wenn Pakete versendet werden, dann kann der FrameCounter verwendet werden, um fehlende Datenpakete zu identifizieren. Da der FrameCounter jedoch auch bei Paketen, welche keine Daten enthalten erhöht werden muss, sonst droht eine wiederverwendung der Nonce, besteht das Problem, dass Datenpakete nicht durchgängig nummeriert sind.

# Idee von Sync von Daten
Für Systeme, welche regelmäßig einen Livedaten verteilen möchten, jedoch nur zu defenierten Zeitpunkten auch die vollständigkeit von Daten prüfen möchten, können Daten auf Anfrage synchronisiert werden. Bei der Synchronisierung fragt das Geräte beim Server nach, welche Datenpakete noch fehlen und sendet diese dann an den Server. 
Für ein ACK kann hierfür die erneute Anfrage nach fehlenden Daten gestellt werden.

# Commands vom Server
Bei LoRaWAN oder NB-IoT Geräten befinden sich Geräte teilweise nur kurz nach dem Versenden von Daten in einem Modus um auch Daten empfangen zu können. Der Server sollte hierbei Anweisungen an Geräte verteilen können, wie SoftwareUpdate verfügbar, Synchronisation Anfragen oder einfach nur Daten. Diese Anfragen müss daher in einer Pipeline hinterlegt und gegebenfalls versendet werden.
Wichtig sind hierbei die Empfangszeiten der Geräte.

# Firmware Update
Es wäre noch interssant ein Firmware Update prozess zu etabliehren, da DIoT primar nur Pakete mit festen Datenstrukturen erlaubt, bei einem Firmwareupdate jedoch mehrere Pakete versendet und empfangen werden müssen.
Bei diesen Paketen muss die Integrität des einzelnen und die Vollständigkeit aller Pakete festgestellt werden.

# Was machte man mit den übrigen 96-Bit Nonce, welche Ascon verwendet?
Auswürfeln und im Handshake austauschen.
Bei Developerpaketen wird die Nonce einfach vollständig verwendet, dass könnte man bei Commandpakten auch machen.
Oder man verwendet mehere Counter, einen für Command und einen für Data

# Warnung bei Schemata
Es sollte eine Warnung geben, wenn ein Schema definiert wird, welches nur RAW Daten defeniert. Das fördert keine Interoperabilität, da diese Daten nicht von anderen verwendet werden können.
Es könnte ein Problem bei Bildern geben, aber auch hier sollte man einen Header definieren.