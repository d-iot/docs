# Beispiel Stromzähler
```
Values:
L1 Voltage 
L2 Voltage
L3 Voltage

L1-L2 Voltage
L2-L3 Voltage
L3-L1 Voltage

L1 Current
L2 Current
L3 Current
N Current

Total active power
L1 active power
L2 active power
L3 active power

Total reactive power
L1 reactive power
L2 reactive power
L3 reactive power

Total power factor 
A power factor 
B power factor 
C power factor 

Frequency
```