# Recommendations
## Random Packages
Es sollten zu den normalen Aufwachzeiten auch zufällige Aufwachzeitpunkte eingeplant werden. Die Verschlüsselung der Pakete schützt nicht vor der Häufigkeits- und Zeitbasierten-Analyse der von dem Gerät ausgehenden Traffic.

Als Beispiel können hier Sensoren, die eine bei der Öffnung eines Fensters oder einer Tür immer nur dann ein Signal senden, wenn diese betätigt werden. So können Angreifer Regelmäßigkeiten in der Nutztung von Sensoren erfassen, ohne selber der Inhalt des Payloads zu kennen. Bei ZigBee oder LoRa/LoRaWAN können durch die eindeutige Message ID die Sensoren genau identifiziert werden.

## Regelmäßige Änderung des SessionKeys

## Sync von Daten

