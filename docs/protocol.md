# Requiered Information and Size
+ DeviceUUID: 128-Bit random UUID - Identify Device globaly
+ DeviceAddress: 32-Bit random ID - Identify Device after Handshake with Server
+ SchemaUUID: 128-Bit random UUID - Idenfify Payload Schema globaly
+ SchemeAddress: 8-Bit incresing ID - Identify supported Schemas of Device after Handshake
+ Version: 8-Bit - Identify Version of Protokoll
+ DataID: 8-Bit - Defines Type of next Data in DEVELOPER-MODE
+ MessageCounter: 32-Bit - Message Counter for Identifing Messages and Inititalize Vector












# Package
| 8-Bit: Version | 24-Bit: RFU | 64-Bit: Device ID | 32-Bit: Message Counter | 128-Bit * n Payload | 128-Bit CMAC |

Minimal Payload Package: 384-Bit or 48 Byte
Minimal Configuration Package: 256-Bit or 32 Byte
Maximal Payload Size: 4.092 Payload Blocks -> 65.472 Bytes (Maximal UDP Package Size)
Recommended Maximal Payload Size: 34 Payload Blocks -> 544 Bytes (Common MTU Size)

Der Payload wir immer auf 128-Bit Blöcke aufgefüllt, daher sollte die Maximale Paketgröße berücksichtigt werden, sowie die Verwendung von passenden Datentypen zur Packetverkleinerung.

## RFU
Antwort des Servers als Konfigurationsflag

# UDP and TCP
TCP für Initialisierung und Synchronisation von Daten

UDP für Live Updates -> Verlustbehaftet


