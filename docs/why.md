# DIoT
Datagramm-Internet-of-Things ist ein Protokoll zum Versenden von strukturierten Daten verschlüsselt direkt zwischen Gerät und Server.

Ziel von DIoT ist es Daten mit möglichst kleinem Paketgrößen zu versenden, um im Low-Power Bereich eingesetzt zu werden.

## Warum sollte ich DIoT verwenden?
### UDP
Im Gegensatz zum MQTT verwendet DIoT UDP, welches es Geräten erlaubt ohne einen Verbindungsaufbau Daten zu verschicken. Durch die Verbindungslosen Kommunikation können Statusmeldungen in nur einem Paket verschicket werden. 
DIoT kümmert sich im Bedarf auch eine Empfangsbestätigungn und eine Synchronisierung verloren geganger Daten.

### Schema
Um Daten ohne Overhead auf der Verbindung verschicken zu können, werden Datenstrukturen in DIoT mit einem Schema definiert, so kann sichergestellt werden, dass nur nutzbare Daten verschicket werden.

### Verschlüsselung
Geräte welche über UDP kommunizieren können nicht einfach mit TLS abgesichert werden, da keine aktive Verbindung aufrecht erhalten werden muss, auch DTLS ist für eine aktive Kommunikation ausgelegt.
Bei DIoT werden Daten mit dem von NIST spezifizierten Standard Ascon-128 verschlüsselt, welche für die Anwendung im IoT Bereich ausgewählt wurde, da sich die Verschlüsselung auch ohne Hardwarebeschleunigung auf Low-Performance Geräten verwenden lässt.

### Geräte-zu-Server Kommunikation
Für die Steuerung von Geräten im Smart Home bieten MQTT und CoAP mit ihrer Broker basierten Strukur ein Vorteil, da Geräte auch untereinander kommunizieren können.
Für die reine Erfassung von Sensordaten und die zentrale Steuerung von Geräten bietet sich eine solche Struktur nicht direkt an, da hierzu die Kommunikation der Geräte untereinander verwaltet werden muss.

### UUID und Public Key
Geräte welche DIoT verwenden, lassen sich über Systeme hinweg durch eine UUID identifizieren.
Die Authentifizierung eines Gerätes wird durch einen Public Key Verfaheren sichergestellt, bei dem der Server die Geräte direkt Authentifizieren kann.

## Wann kann ich DIoT einsetzten?
DIoT bietet sich nicht direkt zur Integration in ein Smart Home System an. Da bei DIoT das Ziel ist eine direkte Verbindung zwischen einem Server und einem Geräte herzustellen.
DIoT bietet sich daher für Sensor Netzwerke und Aktoren mit Zugriffsbeschränkung an, welche nur eine geringe Bandbreite zur Verfügung haben. Auch lassen sich durch die losen Verbindungen und die kurzen Datenpakete besser Low Power Geräte anbinden.
Da nur ein Server direkt mit dem Gerät verbunden sein kann und die Kommunikation verschlüsselt und Authentifiziert erfolgt, lassen sich auch Aktoren anbinden, welche nicht Zugriffsbeschränkt arbeiten.

## Wann sollte ich DIoT einsetzten?
DIoT sollte immer dann eingesetzt werden, wenn Sensordaten erhoben werden.
Optimal lassen sich die Vorteile von DIoT ausnutzten, wenn wenig Bandbreite und nur eine begrenzte Stromversorgung zur Verfügung steht.

DIoT sollte auch dann eingesetzt werden, wenn unterschiedliche Geräte verwendet werden, diese jedoch gleiche Daten erheben.
