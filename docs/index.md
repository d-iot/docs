{!README.md!}

# D-IoT
D-IoT oder auch Data/Datagramm-Internet-of-Things ist ein Protokoll und eine Datenstruktur mit dem Ziel Sensordaten von Geräten möglichst kurz und verschlüsselt zu versenden. 

Das Hauptziel von D-IoT ist es, eine einfache Implementierung bereitzustellen, ohne dabei die Flexibilität zu verlieren.

# Grundsatz
"Sichere und kompakte Datenübertragung, Schluss mit Strings!"

# Ziele
## Paketminimierung
D-IoT nutzt ein Datenschema, um kompakte Pakete zu erstellen und Informationen ohne zusätzlichen Overhead zu übertragen. Dies erleichtert die Integration von Geräten in bestehende Systeme und fördert die Interkonnektivität.

## Globale Schemata
Die Verwendung der Schemadatenstruktur bietet den Vorteil, globale wiederverwendbare Schemata zu erstellen. Dies fördert nicht nur die Interoperabilität zwischen verschiedenen Geräten und Plattformen, sondern unterstützt auch einen Open-Source-Ansatz. Die Verfügbarkeit und gemeinsame Nutzung dieser Schemata erleichtert die Integration von Geräten und fördert die Zusammenarbeit in der IoT-Community, wodurch die Entwicklung und Implementierung von IoT-Lösungen effizienter und offener gestaltet wird.

## Datendokumentation
Schemata bieten den Vorteil, dass sie selbst bei unzureichend dokumentierten IoT-Geräten die Identifizierung von Daten ermöglichen. Selbst wenn die Dokumentation fehlt oder lückenhaft ist, helfen die in den Schemata definierten Datenstrukturen und -typen dabei, die empfangenen Informationen zu erkennen, zu interpretieren und effektiv zu nutzen.

## Wiederverwendbarkeit
Die Entwicklung muss nicht jedes Mal von Grund auf erfolgen. Durch die Bereitstellung öffentlicher Schemata können Entwickler auf bereits bestehende Schemata zurückgreifen, um kompatible Produkte zu entwickeln. Dies fördert die Interoperabilität und den Datenaustausch zwischen verschiedenen Anwendungen und Geräten, spart Zeit und Ressourcen und beschleunigt die Entwicklung von Lösungen im Bereich IoT und anderen Technologiebereichen.

## Geräte-zu-Server Verschlüsselung
In D-IoT erfolgt die Übertragung von Daten immer verschlüsselt, unabhängig davon, über welches Netzwerk die Geräte angeschlossen sind. Die Daten können ausschließlich vom eigenen Server entschlüsselt und validiert werden, wodurch die Sicherheit und Integrität der übertragenen Informationen gewährleistet wird. Dies gewährleistet, dass nur autorisierte Parteien Zugriff auf die Daten haben und schützt sie vor unbefugtem Zugriff und Manipulation während der Übertragung.

## Verbindungslos
Wie der Name D-IoT bereits andeutet, verwendet dieses Protokoll UDP (User Datagram Protocol) und ermöglicht daher eine verbindungslose Kommunikation. Nach der erfolgreichen Anmeldung beim Server können Geräte ihre Sensordaten per UDP an den Server senden, ohne die Notwendigkeit einer dauerhaften aktiven Verbindung oder zusätzlicher Handshake-Prozesse. Dies ermöglicht eine effiziente Übertragung von Daten in IoT-Anwendungen.

## Synchronisierung
Das Senden von Daten ohne Empfangsbestätigung kann zwar den Datenverlust während der Übertragung erhöhen, was jedoch erhebliche Energieeinsparungen ermöglicht. Mit D-IoT wird diese Balance erreicht, indem Echtzeitdaten gesendet werden können, während verlorene Daten im Nachhinein vom Gerät aus zur Vollständigkeit synchronisiert werden können.

## Fehlerbehandlung
In einem fehlerfreien System ermöglicht D-IoT eine schnelle und effiziente Verarbeitung von Paketen zwischen Geräten und dem Server. Dennoch ist es wichtig zu berücksichtigen, dass auch fehlerhafte Sensoren und Daten auftreten können. In solchen Fällen bietet D-IoT die Möglichkeit, Daten als fehlerhaft zu kennzeichnen und sie nicht erneut zu übertragen. Dies trägt dazu bei, Ressourcen zu sparen und die Effizienz der Datenübertragung zu erhöhen.

# Entwicklung
D-IoT befindet sich derzeit noch in der Entwicklungsphase, und es wird intensiv daran gearbeitet, einen vollständigen Stack für den produktiven Einsatz zu entwickeln.

## Analyse
[ ] Bestehnder IoT Protokolle
    [ ] MQTT
    [ ] CoAP
    [ ] LoRaWAN
    [ ] DDS
    [ ] LWM2M
[ ] IoT Netzwerken
    [ ] LoRaWAN
    [ ] Thread
    [ ] NB-IoT
    [ ] LTE/5G
    [ ] ZigBee
[ ] Datenschema
    [ ] Flatbuffer
[ ] Kryptografie
    [ ] AES
    [ ] ChaCha
    [ ] Ascon
    [ ] RSA
    [ ] ECC

## Entwicklung
[ ] Datenstruktur
    [ ] Datentypen
    [ ] Datenparameter
    [ ] Fehlerbehandlung
    [ ] Schema

[ ] Protokollentwicklung
    [ ] Handshake
        [ ] Kryptografie
        [ ] Daten
    [ ] Datenübertragung
    [ ] Empfangsbestätigung
    [ ] Entwicklermodus
    [ ] Synchronisierung

[ ] Software Stack

## Implementierung
[ ] TODO


## Ziele
Für die Entwicklung von D-IoT werden sich folgende Ziele gesetzt:
+ Verkürzung von Paketen auf nur die notwendigen Informationen
+ Verwendung eines Schemas welches die Informationen in einem Paket beschreibt
+ Codegenerierung aus einem Schema zu einem Paket
+ Defenieren eines DeveloperModus, zur Vereinfachung der Entwicklung von neuen Schemas
+ Server-zu-Gerät Verschlüsselung von jedem Paket
+ Validierung von Pakten mit Prüfwert
+ Versenden unterschiedlicher Schema in einem Paket
+ Synchronisation von Daten, bei Paket Verlust
+ Global identifizierbare Pakete und Geräte
+ Speichern von Daten in unbekannten Schema
+ Unabhängigkeit von der Transportebene
+ Zwangsverschlüsselung des Payloads und Zwangsvalidierung

## Was D-IoT nicht können muss
+ Daten in Form von Text übertragen
+ Jeden Datentyp unterstützten
+ Empfangsvalidierung von Pakten beim Server
+ Mesh-Netzwerke erzeugen

## Ziele für die Implementierung
+ Datenbank mit entpakten Paketen zur Auswertung in Grafana
+ Generierung von Code für C/C++ und Python (Geräte und Server)
+ Gernerierung von Arduino Code zum Erstellen von neunen Projekten
+ Webseite zur Veröffentlichung von Schemas

## Probleme die noch gelöst werden müssen
+ Speicherung von AES Keys -> SessionKey
+ Ablehnen von SessionsKeys
+ Rückmeldung von Konfigurationsänderungen an Geräte
+ Symetrische oder Asymetrische Verschlüssung zur Authentifizierung von Geräten zum Server
+ Umgang mit Verlust von Keys