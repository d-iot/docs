

# Base Packet
```
MSB                             LSB
-----------------------------------
| VERSION | Packet Type |   Data  |
|  3-Bit  |    5-Bit    | n-Bytes |
-----------------------------------
```
## Version
```
000: Version 1 - ASCON-128
1XX: Post-quantum cryptography
```

## Packet Types
```
00000: HELLO
00000: HELLO RESPONSE
00000: HELLO CONFIG
00000: DATA
00000: DATA TIME
00000: DATA DEVELOPMENT
```

# Hello Packet
```
MSB                       LSB
-----------------------------
| Base Packet | Device UUID |
|             |   128-Bit   |
-----------------------------
```

# Hello Config Packet
```
MSB                                                                                 LSB
---------------------------------------------------------------------------------------
| Base Packet | Device Address | Frame Counter |      Config Payload      | MAC - TAG |
|             |     32-Bit     |   32-Bit = 0  |  encrypted - 64-Bit * n  |  128-Bit  |
---------------------------------------------------------------------------------------
```

# Config Payload
```
MSB                                                                                                         LSB
---------------------------------------------------------------------------------------------------------------
||  RFU  | Schema Address | Schema UUID |  ................................. || Development Payload - Chained |
|| 1-Bit |      7-Bit     |   128-Bit   |  Repeat until Schema Address is 0  || (optional)            n-Bytes |
---------------------------------------------------------------------------------------------------------------
```


# Data Packet
```
MSB                                                                                 LSB
---------------------------------------------------------------------------------------
| Base Packet | Device Address | Frame Counter | Schema Payload - Chained | MAC - TAG |
|             |     32-Bit     |     32-Bit    |  encrypted - 64-Bit * n  |  128-Bit  |
---------------------------------------------------------------------------------------
```

# Data Time Packet
```
MSB                                                                                             LSB
---------------------------------------------------------------------------------------------------
| Base Packet | Device Address | Frame Counter | Timestamp | Schema Payload - Chained | MAC - TAG |
|             |     32-Bit     |     32-Bit    |   ??-Bit  |  encrypted - 64-Bit * n  |  128-Bit  |
---------------------------------------------------------------------------------------------------

TODO: Bits for Timestamp
```

## Schema Payload
```
MSB                                                                                                LSB
------------------------------------------------------------------------------------------------------
| Error Bit | Schema Address | Error Bitmap (optional) | Struct of Data without FLAGS | FLAGS packed |
|   1-Bit   |      7-Bit     | Schema Length in Bits   |           n-Bytes            |   n-Bytes    |
------------------------------------------------------------------------------------------------------
```

# Data Development Packet
```
MSB                                                                                     LSB
-------------------------------------------------------------------------------------------
| Base Packet | Device Address | Random Nonce | Development Payload - Chained | MAC - TAG |
|             |     32-Bit     |    128-Bit   |     encrypted - 64-Bit * n    |  128-Bit  |
-------------------------------------------------------------------------------------------
```

## Development Payload
```
MSB                                                                                                               LSB
---------------------------------------------------------------------------------------------------------------------
| Schema UUID | Payload Size || Enum  | Array |  RFU  | Data Type | Array Size |   Data   || ...................... |
|    128-Bit  |    16-Bit    || 1-Bit | 1-Bit | 2-Bit |    4-Bit  |    16-Bit  | 1-8 Byte || Repeat to Payload Size |
---------------------------------------------------------------------------------------------------------------------

FLAG Type has no ARRAY
TODO: 128-Bit UINT/SINT/SFLOAT ??? -> Data Size
```


