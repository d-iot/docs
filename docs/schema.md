# Payload Schema
Zur Reduktion der Payload Größe wird ein Schema basierte Komprimierung verwendet. Dazu werden Daten ohne Overhead in einem minimalen Format an der Server gesendet.
Das Schema beschreibt dazu die Größe und Datentypen der einzelnen Felder. Namen und Beschreibungen, sowie Faktoren und Offsets werden nicht in den Payload geladen, sondern nur beim Handshake ausgetauscht.

## Schema ID
Damit jedes Schema eindeutig identifiziert werden kann und jeder sein eigenes Schema definieren kann werden hierfür 128-Bit UUIDs verwendet. Jedes Schema und jede Version einen Schemas MUSS eine neue UUID erhalten, sonst kann es in der Auswertung zu Fehler kommen.
Um bei der Entwicklung von Schemas nicht jedes Mal eine neue ID generieren zu müssen können Daten auch in einem Developer Modus übertragen werden, welcher die einzelnen Felder mit einem Prefix überträgt und der Server legt diese Felder dann gemeinsam in einer Struktur ab.

## Datentypen
Für eine Übertragung von Sensordaten werden KEINE Strings benötigt. Die Datentypen welche in D-IoT defeniert werden dienen der Erfassung von Messdaten und sollten auch so behandelt werden. Für die Übertragung von anderen Sensordaten, wie Bilder oder Videos ist dieses Protokoll nicht geeignet.
Auf Protokollebene werden auch nur grundlegende Datentypen ohne Einheiten (wie SI, etc.) übertragen, diese können in den Schemas definiert werden und werden nur in der Datenbank verwendet.

## Schema Struktur
Schemadatein sollten mehrere Schemas in einer Datei unterstützten.

### UUID
    UUID mit 128-Bits als Version 4 - fully random generated- aus RFC4122

### Name
    String zum Benennen des Datenfeldes, kann vom Entwickler frei verwendet werden.
    Jedoch sollte beachtet werden, dass der Name als Spalten Header in Datenbanken verwendet wird und beim Handshake übertragen wird. Je kürzer der Name ist desto besser.

### Description
    String zur Beschreibung eines Datenfeldes, kann vom Entwickler frei verwendet werden. Auch hierbei auf die Verwendung in der Datenbank achten.
    TODO: Ist es notwendig Beschreibungen auch bei einem Handshake auszutauschen?

### ID
    UINT16

### Datatype
    Siehe Tabelle

    Es sind nur die Werte aus der Tabelle gültig.

### Array
    Hiermit kann der gleiche Datentyp mehrmals definiert werden, Ziel hiervon ist es, dass zusammenhängende Werte auch zusammenhängend in der Datenbank abgelegt, sowie dargestellt werden können.
    Für eine striktere Trennung von Werten ist es sinnvoll ein neues Datenfeld aufzumachen.
    
    Der DEFAULT Wert ist 1: Es ist nur ein Datenfeld vorhanden. 0 ist nicht zulässig und maximal sind 255 Datenfelder zulässig -> 8-Bit

### Factor und Offset
    Float32
    Um Daten auf dem Übertragungsweg soweit wie möglich reduzieren zu können unterstützten Schemas einen Faktor, welcher mit den ankommenden Daten multipliziert wird und einen Offset, welcher auf die Daten addiert wird.
    Der Factor und Offset wird von dem Gerät dividiert, sowie subtrahiert und vom Server wieder multipliziert und addiert. Bei der Implementierung wird erst der Offset abgezogen und dann der Factor betrachtet.

    Der DEFAULT Wert für Offset ist 0.0: Es sind nur Positive Werte erlaubt.
    Der DEFAULT Wert für Factor ist 1.0: Es sind nur Postive Werte und Werte größer als 0 erlaubt. 

### Unit
    Um eine Einheit in der Datenbank zu erstellen kann einen Einheit hinzugefügt werden.

### Range
    Die Range beschreibt den Bereicht in dem sich die Werte befinden können, nachdem sie mit einem Offset und Factor behandelt worden sind. Es wird jeweils ein Maximal und ein Minimalwert angegeben.
    Minimalwert MUSS kleiner sein als der Maximalwert.

### Enum
    Mit einem ENUM können feste Werte in ein String basierten Wert umgewandelt werden. Bei fehlenden Definitionen wird ein UNDEFINED in der Datenbank abgelegt.
    Wenn eine ENUM Liste nicht leer ist, wir jeder Wert ersetzt. 
    Nur bei einer leerenen ENUM Liste wird der Wert direkt in die Datenbank eingetragen.

## Schema Beispiel als JSON
```
{
	"schema": [{
		"uuid": "3faba488-ba4e-441d-9ccd-a632baabca39",
        "name": "Power Measurement - 3 Phases",
		"description": "Power measurement for 3 phases by TheJoKlLa",
		"data": [{
			"id": 0,
			"name": "L1 Voltage",
			"description": "",
			"type": "SINT_32",
			"array": 1,
			"factor": 0.01,
			"offset": 0.0,
			"unit": "V",
			"range": {
				"min": -1000.0,
				"max": 1000.0
			}
		}
    ]
}
```


# Datadefinition Byte
```
| 1 | 2 | 3 | 4 5 6 7 8|
1: Is ENUM?
2: Is ARRAY?
3: RFU
4 5 6 7 8: Datatype
```

# Datatype
```
00: RAW     -   8-Bit
01: UINT    -   8-Bit
02: UINT    -  16-Bit
03: UINT    -  32-Bit
04: UINT    -  64-Bit
05: UINT    - 128-Bit
06: SINT    -   8-Bit
07: SINT    -  16-Bit
08: SINT    -  32-Bit
09: SINT    -  64-Bit
10: SINT    - 128-Bit
11: UFLOAT  -   8-Bit
12: UFLOAT  -  16-Bit
13: UFLOAT  -  32-Bit
14: UFLOAT  -  64-Bit
15: UFLOAT  - 128-Bit
16: SFLOAT  -   8-Bit
17: SFLOAT  -  16-Bit
18: SFLOAT  -  32-Bit
19: SFLOAT  -  64-Bit
20: SFLOAT  - 128-Bit
21: RFU
22: RFU
23: RFU
24: RFU
25: RFU
26: RFU
27: RFU
28: RFU
29: RFU
30: RFU
31: FLAG - 1-Bit wird als 8-Bit Block zusammengefasst
```

UINT - 24-Bit ???
